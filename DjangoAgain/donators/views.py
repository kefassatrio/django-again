from django.shortcuts import render

# Create your views here.
def donator_list(request):
    return render(request, 'donators/donator_list.html')
